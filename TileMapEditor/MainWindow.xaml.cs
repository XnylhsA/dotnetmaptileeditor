﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.Windows.Interactivity;
using Microsoft.Win32;
using System.Collections.ObjectModel;
using System.Data.Common;
using Newtonsoft.Json;
using System.IO;
using System.Collections;

namespace TileMapEditor
{
    public class TileBrush
    {
        public ImageBrush imageBrush { get; set; }
        public int tileID;
        public string ImageId;
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public int water = -2;
        public int grass = -1;
        public int currentTileIndex = 0;
        public int tileSize = 16;
        public int mapHeight = 16, mapWidth = 16;
        public int mapViewerWidth = 768;
        public int mapViewerHeight = 786;
        public int mapViewerWidthMultiplier = 1;
        public int mapViewerHeightMultiplier = 1;
        public DispatcherTimer timer;
        public Board map;
        int leftClick;
        int rightClick;
        Dictionary<int, ImageBrush> tileBrushDictionary;        
        public ObservableCollection<ImportedTile> importedTiles;
        public MapContainer currentMapData;
        public List<Button> TileButtons;
        public MainWindow()
        {
            
            currentMapData = new MapContainer(tileSize, mapHeight, mapWidth);
            importedTiles = new ObservableCollection<ImportedTile>();
            importedTiles.Add(new ImportedTile(grass, "Grass", new BitmapImage(new Uri(@"Grass_Tile.png", UriKind.Relative))));
            importedTiles.Add(new ImportedTile(water, "Water", new BitmapImage(new Uri(@"Water_Tile.png", UriKind.Relative))));
            currentMapData.tileInfo.Add(new TileInfo(grass, "Grass_Tile.png", "Grass"));
            currentMapData.tileInfo.Add(new TileInfo(water, "Water_Tile.png", "Water"));
            InitializeComponent();
            ImageBrush grassTile = new ImageBrush(new BitmapImage(new Uri(@"Grass_Tile.png", UriKind.Relative)));
            ImageBrush waterTile = new ImageBrush(new BitmapImage(new Uri(@"Water_Tile.png", UriKind.Relative)));
            tileBrushDictionary = new Dictionary<int, ImageBrush>();
            tileBrushDictionary.Add(grass, grassTile);
            tileBrushDictionary.Add(water, waterTile);
            leftClick = grass;
            rightClick = water;
            map = new Board(mapHeight, mapWidth, tileBrushDictionary[grass], grass);
            lbImportedTiles.DataContext = importedTiles;
            mapViewerHeight = mapHeight * (int)ZoomSlider.Value * tileSize;
            mapViewerWidth = mapWidth * (int)ZoomSlider.Value * tileSize;
            TileMapContainer.Height = mapViewerHeight;
            TileMapContainer.Width = mapViewerWidth;
            this.DataContext = map;
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(0.5);
            timer.Tick += Timer_Tick;
            timer.Start();
            UpdateButtons();
        }

        public static List<T> GetLogicalChildCollection<T>(object parent) where T : DependencyObject
        {
            List<T> logicalCollection = new List<T>();
            GetLogicalChildCollection(parent as DependencyObject, logicalCollection);
            return logicalCollection;
        }

        private static void GetLogicalChildCollection<T>(DependencyObject parent, List<T> logicalCollection) where T : DependencyObject
        {
            IEnumerable children = LogicalTreeHelper.GetChildren(parent);
            foreach (object child in children)
            {
                if (child is DependencyObject)
                {
                    DependencyObject depChild = child as DependencyObject;
                    if (child is T)
                    {
                        logicalCollection.Add(child as T);
                    }
                    GetLogicalChildCollection(depChild, logicalCollection);
                }
            }
        }
        public void ResizeMap()
        {

        }


        public void Timer_Tick(object sender, EventArgs e)
        {
            UpdateButtons();
        }



        public Byte[] BufferFromImage(BitmapImage imageSource)
        {
            Stream stream = imageSource.StreamSource;
            Byte[] buffer = null;
            if (stream != null && stream.Length > 0)
            {
                using (BinaryReader br = new BinaryReader(stream))
                {
                    buffer = br.ReadBytes((Int32)stream.Length);
                }
            }

            return buffer;
        }

        #region Click Functions

        public void CreateNewMap_Click(Object sender, RoutedEventArgs e)
        {
            NewMapWindow newMap = new NewMapWindow();
            newMap.ShowDialog();
            mapWidth = newMap.widthVal;
            mapHeight = newMap.heightVal;
            tileSize = newMap.tileSize;
            currentMapData = new MapContainer(tileSize, mapHeight, mapWidth);
            importedTiles = new ObservableCollection<ImportedTile>();
            importedTiles.Add(new ImportedTile(grass, "Grass", new BitmapImage(new Uri(@"Grass_Tile.png", UriKind.Relative))));
            importedTiles.Add(new ImportedTile(water, "Water", new BitmapImage(new Uri(@"Water_Tile.png", UriKind.Relative))));
            currentMapData.tileInfo.Add(new TileInfo(grass, "Grass_Tile.png", "Grass"));
            currentMapData.tileInfo.Add(new TileInfo(water, "Water_Tile.png", "Water"));
            ImageBrush grassTile = new ImageBrush(new BitmapImage(new Uri(@"Grass_Tile.png", UriKind.Relative)));
            ImageBrush waterTile = new ImageBrush(new BitmapImage(new Uri(@"Water_Tile.png", UriKind.Relative)));
            tileBrushDictionary = new Dictionary<int, ImageBrush>();
            tileBrushDictionary.Add(grass, grassTile);
            tileBrushDictionary.Add(water, waterTile);
            leftClick = grass;
            rightClick = water;
            map = new Board(mapHeight, mapWidth, tileBrushDictionary[grass], grass);
            lbImportedTiles.DataContext = importedTiles;


            mapViewerHeight = mapHeight * (int)ZoomSlider.Value * tileSize;
            mapViewerWidth = mapWidth * (int)ZoomSlider.Value * tileSize;
            TileMapContainer.Height = mapViewerHeight;
            TileMapContainer.Width = mapViewerWidth;
            this.DataContext = map;
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(0.5);
            timer.Tick += Timer_Tick;
            timer.Start();
            UpdateButtons();
        }

        private void lbImportedTiles_LeftMouseUp(object sender, MouseButtonEventArgs e)
        {
            leftClick = importedTiles[lbImportedTiles.SelectedIndex].imageID;
        }
        private void lbImportedTiles_RightMouseUp(object sender, MouseButtonEventArgs e)
        {
            rightClick = importedTiles[lbImportedTiles.SelectedIndex].imageID;
        }
        private void OpenMapButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text Files (*.json)|*.json";
            if (openFileDialog.ShowDialog() == true)
            {

                string MapJson = File.ReadAllText(openFileDialog.FileName);
                currentMapData = JsonConvert.DeserializeObject<MapContainer>(MapJson);
                if (currentMapData == null)
                    return;

                string directory = System.IO.Path.GetDirectoryName(openFileDialog.FileName);
                if(System.IO.Directory.Exists(System.IO.Path.Combine(directory, "Tile_Images")))
                {
        
                    foreach (var item in currentMapData.tileInfo)
                    {
                        string targetImagePath = System.IO.Path.Combine(directory, item.ImagePath);
                        ImportedTile temp = new ImportedTile(item.symbol, item.name, new BitmapImage(new Uri(@targetImagePath)));
                        if(!tileBrushDictionary.ContainsKey(temp.imageID))
                        {
                            tileBrushDictionary.Add(temp.imageID, temp.imageBrush);

                        }
                        importedTiles.Add(temp);

                    }

                    mapHeight = currentMapData.MapHeight;
                    mapWidth = currentMapData.MapWidth;
                    map = new Board(mapHeight, mapWidth, tileBrushDictionary[grass], grass);
                    map.Tiles.Clear();
                    mapViewerHeight = mapHeight * (int)ZoomSlider.Value * tileSize;
                    mapViewerWidth = mapWidth * (int)ZoomSlider.Value * tileSize;

                    TileMapContainer.Height = mapViewerHeight;
                    TileMapContainer.Width = mapViewerWidth;
                    for (int r = 0; r < mapHeight; r++)
                    {
                        for (int c = 0; c < mapWidth; c++)
                        {

                            int calculatedIndex = (r * map.Columns) + c;
                            int tileID = currentMapData.mapData[calculatedIndex];
                            map.Tiles.Add(new Tile(r, c, tileBrushDictionary[tileID], tileID));
                            
                        }
                    }

                    this.DataContext = map;
                    UpdateButtons();

                }

            }
        }
        void UpdateButtons()
        {
            foreach (Button item in FindVisualChildren<Button>(TileMapContainer))
            {
                Tile selectedTile = (Tile)item.DataContext;
                int r = selectedTile.Row;
                int c = selectedTile.Col;
                item.Background = map.Tiles[(r * map.Columns) + c].Background;
            }
        }


        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        private void OpenTilesButton_Click(object sender, RoutedEventArgs e)
        {
            if(currentTileIndex >= 2147483647)
            {
                return;
            }
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            openFileDialog.Filter = "Image files (*.png)|*.png";
            if (openFileDialog.ShowDialog() == true)
            {
                foreach (var image in openFileDialog.FileNames)
                {
                    string name = System.IO.Path.GetFileNameWithoutExtension(image);
                    currentMapData.tileInfo.Add(new TileInfo(currentTileIndex, image, name));
                    ImportedTile temp = new ImportedTile(currentTileIndex,name, new BitmapImage(new Uri(image)));
                    tileBrushDictionary.Add(temp.imageID, temp.imageBrush);
                    importedTiles.Add(temp);
                    if (currentTileIndex < 2147483647)
                    {
                        currentTileIndex++;
                    }
                    else
                        return;
                }

            }
        }

        private void SaveMapButton_Click(object sender, RoutedEventArgs e)
        {
            string mapDirectory;
            string tileImageDirectoryPath;
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Json Files (*.json)|*.json";
            if (saveFileDialog.ShowDialog() == true)
            {
                mapDirectory = System.IO.Path.GetDirectoryName(saveFileDialog.FileName);

                DirectoryInfo tileImageDirectory = Directory.CreateDirectory(System.IO.Path.Combine(mapDirectory + "/Tile_Images"));
                tileImageDirectoryPath = tileImageDirectory.FullName;
                int id = currentMapData.maxTile;

                foreach (var item in currentMapData.tileInfo)
                {
                    if(item.saved == false)
                    {
                        string newPath = System.IO.Path.Combine(tileImageDirectoryPath, "Tile_" + id + ".png");
                        File.Copy(item.ImagePath, newPath, true);
                        item.ImagePath = tileImageDirectory.ToString() + "/Tile_" + id + ".png";
                        id++;
                        item.saved = true;
                    }
                }
                currentMapData.maxTile = id;

                currentMapData.mapData = new int[mapHeight * mapWidth];
                for (int row = 0; row < mapHeight; row++)
                {
                    for (int col = 0; col < mapWidth; col++)
                    {
                        currentMapData.mapData[row * map.Columns + col] = map.Tiles[(row * map.Columns) + col].imageID;
                    }
                }
                string output = JsonConvert.SerializeObject(currentMapData);


                StreamWriter sw = new StreamWriter(saveFileDialog.FileName);
                sw.Write(output);
                sw.Close();
            }

        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            (sender as Button).ContextMenu.IsEnabled = true;
            (sender as Button).ContextMenu.PlacementTarget = (sender as Button);
            (sender as Button).ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            (sender as Button).ContextMenu.IsOpen = true;
            UpdateButtons();
        }
        private void TileButton_LeftMouseUp(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            Tile selectedTile = (Tile)btn.DataContext;
            int r = selectedTile.Row;
            int c = selectedTile.Col;
            map.Tiles[(r * map.Columns) + c].Background = tileBrushDictionary[leftClick];

            map.Tiles[(r * map.Columns) + c].imageID = leftClick;
            UpdateButtons();

        }
        private void TileButton_RightMouseUp(object sender, MouseButtonEventArgs e)
        {
            Button btn = sender as Button;
            Tile selectedTile = (Tile)btn.DataContext;
            int r = selectedTile.Row;
            int c = selectedTile.Col;
            map.Tiles[(r * map.Columns) + c].Background = tileBrushDictionary[rightClick];

            map.Tiles[(r * map.Columns) + c].imageID = rightClick;
            UpdateButtons();

        }

        private void ZoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            mapViewerHeight = mapHeight * (int)ZoomSlider.Value * tileSize;
            mapViewerWidth = mapWidth * (int)ZoomSlider.Value * tileSize;
            TileMapContainer.Height = mapViewerHeight;
            TileMapContainer.Width = mapViewerWidth;
        }

        private void TileButton_MouseMove(object sender, RoutedEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                Button btn = sender as Button;
                Tile selectedTile = (Tile)btn.DataContext;
                int r = selectedTile.Row;
                int c = selectedTile.Col;
                map.Tiles[(r * map.Columns) + c].Background = tileBrushDictionary[leftClick];

                map.Tiles[(r * map.Columns) + c].imageID = leftClick;
                UpdateButtons();
            }
            if(Mouse.RightButton == MouseButtonState.Pressed)
            {
                Button btn = sender as Button;
                Tile selectedTile = (Tile)btn.DataContext;
                int r = selectedTile.Row;
                int c = selectedTile.Col;
                map.Tiles[(r * map.Columns) + c].Background = tileBrushDictionary[rightClick];

                map.Tiles[(r * map.Columns) + c].imageID = rightClick;
                UpdateButtons();

            }

        }
        #endregion

    }

    public class RollOverBehavior : Behavior<Button>
    {
        bool mouseOver;
        bool clicked;

        protected override void OnAttached()
        {
            AssociatedObject.PreviewMouseLeftButtonDown += (s, e) =>
            {
                e.Handled = true;
            };
            AssociatedObject.MouseEnter += (s, e) =>
            {
                mouseOver = true;
                clicked = false;
            };
            AssociatedObject.MouseLeave += (s, e) =>
            {
                mouseOver = false;
            };
            AssociatedObject.MouseMove += (s, e) =>
            {
                if (mouseOver && !clicked && e.LeftButton == MouseButtonState.Pressed)
                {
                    clicked = true;
                }
            };
        }
    }

    public class Board
    {
        int _rows;
        int _columns;
        List<Tile> _tiles = new List<Tile>();

        public Board(int rows, int columns, ImageBrush baseTile, int id)
        {
            _rows = rows;
            _columns = columns;
            Random random = new Random();

            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < columns; c++)
                {
                    var grassimage = Properties.Resources.GrassTile;
                    _tiles.Add(new Tile(r,c, baseTile, id));

                }
            }
        }

        public int Rows
        {
            get { return _rows; }
            set { _rows = value; }
        }

        public int Columns
        {
            get { return _columns; }
            set { _columns = value; }
        }

        public List<Tile> Tiles
        {
            get { return _tiles; }
            set { _tiles = value; }
        }
    }

    public class Tile
    {
        public string Data { get; set; }
        public int Row { get; set; }
        public int Col { get; set; }
        public ImageBrush Background { get; set; }
        public int imageID = -1;
        
        public Tile(string data, ImageBrush imgSource, int imgId)
        {
            
            Data = data;
            imageID = imgId;
            Background = imgSource;
        }
        public Tile(int row, int col, ImageBrush imgSource, int imgId)
        {
            Row = row;
            Col = col;
            Data = string.Format("row {0}, col {1}", Row, Col);
            imageID = imgId;
            Background = imgSource;
            
        }
    }

    public class ImportedTile
    {
        public ImportedTile(int imgId, string title, ImageSource imageSource)
        {
            Title = title;
            Image = imageSource;
            imageBrush = new ImageBrush(imageSource);
            imageID = imgId;
            
        }
        public string Title { get; set; }
        public ImageSource Image { get; set; }
        public ImageBrush imageBrush { get; set; }
        public int imageID;

    }

    public class MapContainer
    {
        public int TileSize;
        public int MapHeight;
        public int MapWidth;
        public int[] mapData;
        public List<TileInfo> tileInfo;
        public int maxTile = 0;
        public MapContainer(int tileSize, int height, int width)
        {
            TileSize = tileSize;
            MapHeight = height;
            MapWidth = width;
            tileInfo = new List<TileInfo>();
        }
    }

    public class TileInfo
    {
        public int symbol;
        public string ImagePath;
        public bool saved = false;
        public string name = "unintialized";
        public TileInfo(int tileId, string imgPath, string tileName)
        {
            name = tileName;
            symbol = tileId;
            ImagePath = imgPath;
        }
     
    }


}
