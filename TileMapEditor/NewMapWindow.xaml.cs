﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TileMapEditor
{
    /// <summary>
    /// Interaction logic for NewMapWindow.xaml
    /// </summary>
    public partial class NewMapWindow : Window
    {
        public NewMapWindow()
        {
            InitializeComponent();
        }
        public int widthVal = 16;
        public int heightVal = 16;
        public int tileSize =16;


        public void CreateButton_Clicked(object sender, RoutedEventArgs e)
        {

            if (int.TryParse(WidthTextBox.Text, out widthVal))
            {
                
            }
            else
            {
                MessageBox.Show("Hey, we need an int over here.");
                return;
            }
            if (int.TryParse(HeightTextBox.Text, out heightVal))
            {
                
            }
            else
            {
                MessageBox.Show("Hey, we need an int over here.");
                return;

            }
            if (int.TryParse(TileSizeTextBox.Text, out tileSize))
            {

            }
            else
            {
                MessageBox.Show("Hey, we need an int over here.");
                return;

            }
            if (widthVal < 1 || heightVal < 1 || tileSize < 2 || widthVal > 64 || heightVal > 64)
                return;
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
